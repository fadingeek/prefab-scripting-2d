## Prefab Scripting - 2D

**THANK YOU SO MUCH FOR SUPPORTING THE WORK. YOU ARE AWESOME :)**

**Note**: This Asset is a TOOL and not a resource pack.

# Short Description

Prefab Scripting 2D version is a combination of the following tools:-

**2D character** : a 2D platformer character.

**2D Shooter** : a 2D platformer character with shooting feature.

**Destroyer** : Destroys an object with respect to delay, on start and many other conditions.

**MouseFollower** : An object which follows the mouse.

**ObjectFollower** : An object which follows another object(if the object has transform and is draged and dropped into the script).

**PathFollower** : An object which follows a path(given the 2 object which will be considered as start and end of the path).

**LookAt** : Rotates and looks at the object specified.

**MovingPlatform** : A Platform which moves in a given path.

**SetMovement** : Moves from a point to a direction provided.Also has the ability to destroy itself on delay after some time.(can be disabled)

**2D Spawner** : Spawns an object along x and y axis after a delay with a uniform or non-unirform time gaps in between.

# Long Description

Prefab Scripting is a simple to use set of tools to be used. It's made with customization, and effeciency kept in mind for the user to have good experience.

Here are the documentation on the all the tools:-

1. ** 2D character ** :
    2D character is basically an animated sprite which can move in multiple direcetions on input. 
    The Following options can be found in the Player 2D script.
   
   1. Jump Force: The height of a jump by the player
   2. Movement Speed: The speed of the movement of the player.
   3. It has the ability to flip on moving in different directions. This can be turned off. by unchecking the "Flip Turn" option. 
   4. (Not Recommended): Animator: To change the jump animation of the player.
      Other than these there are some customization options which is highly recommended. Customizing the following is highly recmommended.
   5. the sprite.
   6. The Looks.
   7. the Left and right movement animation

2. 2D player Shooter:-
    2D Shooter is simply a 2D character but with an extra ability to shoot:-
    The Following options can be found in the Player Shooter script:-
    (NOTE: the default bullet uses the "set movement" tool to move)
   
   1. Bullet: A game object/prefab which should be dragged and dropped into the script which will
      be considered as the bullet will be shot.
   2. FirePoint: A game object's position from which the bullet will be shot.(The game object should have transform component)
   3. Bullet Prefab/SetMovement: Destroy time delay, Speed, etc.
      Recommended:-
   4. The gun sprite
   5. bullet
   6. player Looks

3. Destroyer:-
    As the name suggests, Destroyer destroys an object with some conditions.
    The Following options can be found in the Player Shooter script:-
   
   1. Object to destroy: drag and drop the object which you wish to be destroyed in the game.
   2. Destroy Time: destroy a game object on a delay of time given.(works inly if "Destroy on delay" bool is checked)
   3. Destroy on start:(Not Recommended) destroys an object on game start. Can be done with some delay.(delay works inly if "Destroy on delay" bool is checked)

4. Mouse Follower:-
   made to follow the mouse cursor during the game.
   Customizable:-
   
   1. sprite which follows the mouse
   2. the game object following mouse

5. Object Follower:-
    Object Follower is a tool which should be used when you want an object to follow another object.
   
   1. Target: The object which will be followed by the Object Follower.
   2. Move Speed: Speed through which the object is being followed by the object follower
      Customize:-
   3. The default Arrow.
   4. speed
   5. The target

6. PathFollower:-
    Provided the start and the end point, an object can be made go from start to end From this asset.
   
   1. Start: The transform position from which the follower starts.
   2. End: The transform position to which the follower end.
   3. Follower: The object from goes from staart to end
      For all the three, drag the an object which has the transform you dezire.

7. LookAt:-
    Look At has the ability to to rotate towards and look towards any object
   
   1. Player: The Object which is going to be used as target to look towards.
      Customize:-
   2. The player object.

8. MovingPlatform:-
    This is a more customized fork of the path follower. 

9. SetMovement:-
    This is a tool which can come handy in many cases if used properly. It sets movement For any 2D game object
   
   1. Object To move: The objects you wish to movement
   2. X and Y bool can should be checked based which direction the 2d object is moving.
   3. Destroy bool  should checked if the game object should be destroyed after some time.
   4. Delay Time: Number of seconds after which the the moving object will be destroyed.
      Uses:-
   5. Bullet: Destroys after some time.
   6. Endless runner, which rewuires movement only in one direction.
      Customizations:-
   7. The object used for the game.
   8. Looks
   9. Delay time as you desire

10. 3D Spawner
    Spawns a pirticular object in a range  you specify
    
    1. Object To Spawn: The game object which would be spawned(which you should specify)
    2. Start Spawn Delay: Time After which the spawn starts 
    3. - RandomizeRespawnTimeGaps: Should be false if you want uniform time between two spawns. This should be checked true if you want a non-uniform random time with set range, between two spawns.
       - Respawn Time Gap: The time gap between 2 spawns of the object. This works only if RandomizeRespawnTimeGaps is false. This lets you have uniform time between two spawns.
       - RespawnTimeRange1 & RespawnTimeRange2: This works only if RandomizeRespawnTimeGaps is True. A random float number between RespawnTimeRange1 & RespawnTimeRange2 will be chosen as a time gap between two spawns each time.
    4. The random value of x coordinate will be chosen throught the range given from x1 to x2
    5. The random value of y coordinate will be chosen throught the range given from y1 to y2

## Conclusion

Well... If you have used the asset you would propably realize that the tools does not "look" good. It is really not made for that. It is made for simply made to save your time. Example: It saves time by giving you the whole framework of a menu, but it also encorages you to try out customizing it yourself as well.

If you have furthur problems, 
head hover to FadinGeek Channel where there will be videos posted.
You'll find the link to that in the help tab in unity.

That's about it... 
I hope you enjoy using this asset as much as we enjoyed making it :)
Thank You 
## . . . FadinGeek . . .
